class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name

    def addIngredient(self, ingredient: Ingredient):
        pass

    def numIngredients(self) -> int:
        pass

    def computePrice(self) -> int:
        pass


class Order:
    def getPizza(self, position: int) -> Pizza:
        pass

    def initializeOrder(self, numPizzas: int):
        pass

    def addPizza(self, pizza: Pizza):
        pass

    def numPizzas(self) -> int:
        pass

    def computeTotal(self) -> int:
        pass


class Pizzeria:
    def addIngredient(self, name: str, description: str, price: int):
        pass

    def findIngredient(self, name: str) -> Ingredient:
        pass

    def addPizza(self, name: str, ingredients: []):
        pass

    def findPizza(self, name: str) -> Pizza:
        pass

    def createOrder(self, pizzas: []) -> int:
        pass

    def findOrder(self, numOrder: int) -> Order:
        pass

    def getReceipt(self, numOrder: int) -> str:
        pass

    def listIngredients(self) -> str:
        pass

    def menu(self) -> str:
        pass
