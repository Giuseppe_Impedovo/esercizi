#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include<sstream>
#include <algorithm>
#include <fstream>

using namespace std;

namespace PizzeriaLibrary {

    class Ingredient
    {
        public:
            string Name;
            int Price;
            string Description;
        public:
            Ingredient(const string& name, const int& price, const string& description){Name = name; Price = price; Description = description;}
            Ingredient& operator =(const Ingredient& ingredient)
            {
                Name = ingredient.Name;
                Price = ingredient.Price;
                Description = ingredient.Description;
                return *this;
            }
    };

    class Pizza
    {
        public:
            string Name;
            list<Ingredient> Ingredients;
        public:
            Pizza(const string& name, const list<Ingredient> ingredients){Name = name; Ingredients = ingredients;}
            Pizza();

            void AddIngredient(const Ingredient& ingredient) { Ingredients.push_back(ingredient); }
            int NumIngredients() const { return Ingredients.size(); }
            int ComputePrice() const
            {
                unsigned int price = 0;
                unsigned int tmpPrice;
                list<Ingredient> tmpList;
                for(unsigned int i = 0; i < Ingredients.size(); i++)
                {
                    tmpPrice = tmpList.end()->Price;
                    price += tmpPrice;
                    tmpList.pop_back();
                }
                return price;
            }
    };

    class Order
    {
        public:
            list <Pizza> Pizzas;
            int NumOrders;
         public:
            void InitializeOrder(int numPizzas, int numOrder)
            {
                Pizzas.resize(numPizzas);
                NumOrders = numOrder;
            }
            void AddPizza(const Pizza& pizza)
            {
                Pizzas.push_back(pizza);
            }
            const Pizza& GetPizza(const int& position)
            {

                if(position >= Pizzas.size() || position == 0)
                    throw runtime_error("Position passed is wrong");
                else
                {
                    int i = -1;
                    for(list<Pizza>::iterator it = Pizzas.begin(); it != Pizzas.end(); it++)
                    {
                        i++;
                        if(i == position - 1)
                        {
                            return *it;
                        }
                    }
                }
            }
            int NumPizzas() const
            {
                return Pizzas.size();
            }
            int ComputeTotal() const
            {
                unsigned int price = 0;
                list<Pizza>tmpList2 = Pizzas;
                for(unsigned int i = 0; i < Pizzas.size(); i++)
                {
                    price += tmpList2.end()->ComputePrice();
                    tmpList2.pop_back();
                }
                return price;
            }
    };

    class Pizzeria
    {
        public:
            list<Ingredient> ingredients;
            list<Pizza> pizzas;
            list<Order> orders;
        public:
            void AddIngredient(const string& name,
                               const string& description,
                               const int& price)
            {
                int flag = 0;
                for(list<Ingredient>::iterator it = ingredients.begin();it != ingredients.end(); it++)
                {
                    if(it->Name == name)
                    {
                        flag = 1;
                    }
                }
                if(flag == 1)
                    throw runtime_error("Ingredient already inserted");
                else
                {
                    Ingredient newIngredient = Ingredient(name, price, description);
                    ingredients.push_back(newIngredient);
                }
            }

            const Ingredient& FindIngredient(const string& name)
            {
                for(list<Ingredient>::iterator it = ingredients.begin();it != ingredients.end(); it++)
                {
                    if(it->Name == name)
                        return *it;
                }
                  throw runtime_error("Ingredient not found");
            }

            void AddPizza(const string& name,const vector<string>& ingredients)
            {
                for(list<Pizza>::iterator it = pizzas.begin(); it != pizzas.end(); it++)
                {
                    if(name == it->Name)
                        throw runtime_error("Pizza already inserted");
                }
                int iSize = ingredients.size();
                list<Ingredient> tmpList;
                for(int i = 0; i < iSize; i++)
                    tmpList.push_back(FindIngredient(ingredients[i]));
                Pizza newPizza = Pizza(name, tmpList);
                pizzas.push_back(newPizza);

            }

            const Pizza& FindPizza(const string& name)
            {
                for(list<Pizza>::iterator it = pizzas.begin(); it != pizzas.end(); it++)
                    {
                        if(it->Name == name)
                            return *it;
                    }
                throw runtime_error("Pizza not found");
            }

            int CreateOrder(const vector<string>& pizzas)
            {
                if(pizzas.size() == 0)
                    throw runtime_error("Empty order");
                unsigned int dimOrder = orders.size();
                Order newOrder;
                for(list<Pizza>::iterator it = newOrder.Pizzas.begin(); it != newOrder.Pizzas.end(); it++)
                {
                    for(unsigned int i = 0; i < pizzas.size(); i++)
                        it->Name = pizzas[i];
                }
                newOrder.NumOrders = 1000 + dimOrder;
                orders.push_back(newOrder);
                return newOrder.NumOrders;
            }

            const Order& FindOrder(const int& numOrder)
            {
                for(list<Order>::iterator it = orders.begin(); it != orders.end(); it++)
                {
                    if(it->NumOrders == numOrder)
                        return *it;
                }
                    throw runtime_error("Order not found");
            }

            string GetReceipt(const int& numOrder)
            {
                string receipt = 0;
                unsigned int ingPrice;
                unsigned int total_;
                Order fOrder = Pizzeria::FindOrder(numOrder);
                for(list<Pizza>::iterator it = fOrder.Pizzas.begin(); it != fOrder.Pizzas.end(); it++)
                {
                    ingPrice = 0;
                    unsigned int i = 0;
                    Pizza fPizza = FindPizza(it->Name);
                    for(list<Ingredient>::iterator it = fPizza.Ingredients.begin(); it != fPizza.Ingredients.end(); it++)
                    {
                        Ingredient fIngredient = FindIngredient(it->Name);
                        ingPrice = ingPrice + fIngredient.Price;
                    }
                    if(i == 0)
                        receipt = "- " + fPizza.Name + ", " + to_string(ingPrice) + " euro" + "\n";
                    else
                        receipt = receipt + "- " + fPizza.Name + ", " + to_string(ingPrice) + " euro" + "\n";
                    total_ = total_ + ingPrice;
                    i++;
                }
                receipt = receipt + " TOTAL: " + to_string(total_) + " euro" + "\n";
                return receipt;
            }

            string ListIngredients()
            {
                vector<string> nameIngredients;
                string ordList;
                for(unsigned int i = 0; i < ingredients.size(); i++)
                    {
                        for(list<Ingredient>::iterator it = ingredients.begin(); it != ingredients.end(); it++)
                            nameIngredients[i] = it->Name;
                    }
                sort(nameIngredients.begin(), nameIngredients.end());
                for(unsigned int i = 0; i < ingredients.size(); i++)
                {
                    for(list<Ingredient>::iterator it = ingredients.begin(); it != ingredients.end(); it++)
                    {
                        if(nameIngredients[i] == it->Name)
                        {
                            if(i == 0)
                                ordList = it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + "\n";
                            else
                                ordList = it->Name + " - '" + it->Description + "': " + to_string(it->Price) + " euro" + "\n" + ordList;
                        }
                    }
                }
                return ordList;
             }

            string Menu()
            {
                string menu;
                int i = -1;
                for(list<Pizza>::iterator it = pizzas.begin(); it != pizzas.end(); it++)
                {
                    i++;
                    Pizza newPizza = *it;
                    unsigned int numberIngredients = newPizza.NumIngredients();
                    unsigned int pricePizza = newPizza.ComputePrice();
                    if(i == 0)
                         menu = it->Name + " (" + to_string(numberIngredients) + " ingredients): " + to_string(pricePizza) + " euro" + "\n";
                    else
                        menu = it->Name + " (" + to_string(numberIngredients) + " ingredients): " + to_string(pricePizza) + " euro" + "\n" + menu;
                }
                return menu;
            }
    };
}
#endif // PIZZERIA_H
