#include "Intersector2D2D.h"


Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D2D::NoIntersection;
}


Intersector2D2D::~Intersector2D2D()
{

}

void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
}


void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;
}

bool Intersector2D2D::ComputeIntersection()
{
    tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
    double parallelism = tangentLine.squaredNorm();
    double check = toleranceParallelism * toleranceParallelism * matrixNomalVector.row(0).squaredNorm() *  matrixNomalVector.row(1).squaredNorm();
    if(parallelism * parallelism <= check)
        if (abs(rightHandSide(1) - rightHandSide(0)) < toleranceIntersection)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoIntersection;
            return false;
        }
    else
    {
        rightHandSide(2) = 0.00;
        matrixNomalVector.row(2) = tangentLine;
        Vector3d pointLine = matrixNomalVector.fullPivLu().solve(rightHandSide);
        intersectionType = LineIntersection;
        return true;
    }
    return false;
}
