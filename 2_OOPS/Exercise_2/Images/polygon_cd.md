```plantuml
@startuml


class Point {
  +X: double
  +Y: double
}

interface IPolygon{
  +{abstract} double Area()
}

class Ellipse{
  +Ellipse(Point center, int a, int  b)
}

class Circle{
  +Circle(Point center, int  radius)
}

class Triangle{
  +Triangle(Point p1, Point p2, Point p3)
}

class TriangleEquilateral{
  +Triangle(Point 1, int edge)
}

class Quadrilateral{
  +Quadrilateral(Point p1, Point p2, Point p3, Point p4)
}

class Parallelogram{
  +Parallelogram(Point p1, Point p2, Point p4)
}
class Rectangle{
  +Rectangle(Point p1, int base, int height)
}
class Square{
  +Rectangle(Point p1, int edge)
}

Point "*" --* "1" IPolygon : has
IPolygon <|.. Ellipse  : implements
IPolygon <|.. Triangle  : implements
IPolygon <|.. Quadrilateral  : implements
Ellipse <|-- Circle  : is an
Triangle <|-- TriangleEquilateral  : is a
Quadrilateral <|-- Parallelogram  : is a
Parallelogram <|-- Rectangle  : is a
Rectangle <|-- Square  : is a


@enduml
```