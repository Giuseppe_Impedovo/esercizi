# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    _numberBuses = 0;
    _buses.clear();

    ifstream file;
    file.open(_busFilePath.c_str());

    if (file.fail())
      throw runtime_error("Something goes wrong");

    try {
      string line;
      getline(file, line);
      getline(file, line);
      istringstream convertN;
      convertN.str(line);
      convertN >> _numberBuses;

      _buses.resize(_numberBuses);
      getline(file, line);
      getline(file, line);
      for (int i = 0; i < _numberBuses; i++)
      {
        getline(file, line);
        istringstream converterfuel;
        converterfuel.str(line);
        converterfuel >> _buses[i].Id >> _buses[i].FuelCost;
      }

      file.close();
    } catch (exception) {
      _numberBuses = 0;
      _buses.clear();

      throw runtime_error("Something goes wrong");
    }
}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if (idBus > _numberBuses)
      throw runtime_error("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}

void MapData::Load()
{
    Reset();

    ifstream file;
    file.open(_mapFilePath.c_str());

    if (file.fail())
      throw runtime_error("Something goes wrong");

    try {
      string line;

      getline(file, line);
      getline(file, line);
      istringstream convertBusStops;
      convertBusStops.str(line);
      convertBusStops >> _numberBusStops;

      getline(file, line);
      _busStops.resize(_numberBusStops);
      for (int i = 0; i < _numberBusStops; i++)
      {
        getline(file, line);
        istringstream converter;
        converter.str(line);
        converter >> _busStops[i].Id >> _busStops[i].Name >> _busStops[i].Latitude >> _busStops[i].Longitude;
      }

      getline(file, line);
      getline(file, line);
      istringstream convertNumStreets;
      convertNumStreets.str(line);
      convertNumStreets >> _numberStreets;

      getline(file, line);
      _streets.resize(_numberStreets);
      _streetsFrom.resize(_numberStreets);
      _streetsTo.resize(_numberStreets);
      for (int i = 0; i < _numberStreets; i++)
      {
        getline(file, line);
        istringstream converterStreets;
        converterStreets.str(line);
        converterStreets >> _streets[i].Id >> _streetsFrom[i] >> _streetsTo[i] >> _streets[i].TravelTime;
      }

      getline(file, line);
      getline(file, line);
      istringstream converterNumRoutes;
      converterNumRoutes.str(line);
      converterNumRoutes >> _numberRoutes;

      getline(file, line);
      _routes.resize(_numberRoutes);
      _routeStreets.resize(_numberRoutes);
      for (int i = 0; i < _numberRoutes; i++)
      {
        getline(file, line);
        istringstream converterRoutes;
        converterRoutes.str(line);
        converterRoutes >> _routes[i].Id >> _routes[i].NumberStreets;
        _routeStreets[i].resize(_routes[i].NumberStreets);

        for (int j = 0; j < _routes[i].NumberStreets; j++)
          converterRoutes >> _routeStreets[i][j];
      }

      file.close();
    } catch (exception) {
      Reset();

      throw runtime_error("Something goes wrong");
    }
  }

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if (idRoute > _numberRoutes)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    const Route& route = _routes[idRoute - 1];

    if (streetPosition >= route.NumberStreets)
      throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    int idStreet;
    idStreet = _routeStreets[idRoute - 1][streetPosition];

    return _streets[idStreet - 1];
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if (idRoute > _numberRoutes)
      throw runtime_error("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idFrom = _streetsFrom[idStreet - 1];

    return _busStops[idFrom - 1];
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if (idStreet > _numberStreets)
      throw runtime_error("Street " + to_string(idStreet) + " does not exists");

    int idTo = _streetsTo[idStreet - 1];

    return _busStops[idTo - 1];
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if (idBusStop > _numberBusStops)
      throw runtime_error("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int travelTime = 0;
    for (int s = 0; s < route.NumberStreets; s++)
      travelTime += _mapData.GetRouteStreet(idRoute, s).TravelTime;

    return travelTime ;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    const Bus& bus = _busStation.GetBus(idBus);
    const int& travelTime = ComputeRouteTravelTime(idRoute);
    int cost = bus.FuelCost * BusAverageSpeed * travelTime / 3600; //tempo da sec a h
    return cost;
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    int s = 0;
    ostringstream routeView;
    routeView<< to_string(idRoute)<< ": ";
    for (; s < route.NumberStreets - 1; s++)
    {
      int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      routeView << from<< " -> ";
    }
      int idStreet = _mapData.GetRouteStreet(idRoute, s).Id;
      string from = _mapData.GetStreetFrom(idStreet).Name;
      string to = _mapData.GetStreetTo(idStreet).Name;
      routeView << from<< " -> "<< to;

      return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) + ": " + from.Name + " -> " + to.Name;
}
string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name + " (" + to_string((double)busStop.Latitude / 10000.0) + ", " + to_string((double)busStop.Longitude / 10000.0) + ")";
}

}
